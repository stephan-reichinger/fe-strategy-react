# HOW TO START #


- run npm install

- launch db json server in directory src/json
```
 json-server --watch db.json
```

- start the application with 
```
 ./node_modules/.bin/webpack -d --watch
```