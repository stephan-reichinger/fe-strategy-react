import React from 'react';
import RecipeList from './Recipes/RecipeListComponent.js';
import ContentHeader from './ContentHeaderComponent.js';

class ContentComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.componentDidMount = this.componentDidMount.bind(this);
    };

    loadCommentsFromServer () {
        fetch('http://localhost:3000/recipes')
            .then(response => response.json())
            .then(data => this.setState({data: data}))
            .catch(err => console.error(this.props.url, err.toString()))
    };


    componentDidMount () {
        console.log('component Mounted')
        this.loadCommentsFromServer();
    };

    render() {
        return (
            <div className="content-wrapper">
                <ContentHeader />
                <RecipeList data={this.state.data} />
            </div>
        );
    }
}

export default ContentComponent;