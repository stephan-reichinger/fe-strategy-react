import React from 'react';

class ContentHeaderComponent extends React.Component{
    render() {
        return (
            <div className="content-header">
                <form id="search">
                    <input type="text" id="searchfield" name="searchfield" />
                        <button type="submit" form="search" id="search-submit" name="search-submit">Search</button>
                </form>
                <form id="sort">
                    Sort by:
                    <select>
                        <option value="date">Date</option>
                        <option value="alphabetical">Alphabetical order</option>
                        <option value="rating">Rating</option>
                    </select>
                </form>
            </div>
        );
    }
}

export default ContentHeaderComponent;