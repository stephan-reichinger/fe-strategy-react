import React from 'react';
import {Router, Route , Link} from 'react-router'

class NavbarComponent extends React.Component{
    render() {
        return (
            <header>
                <h1>We love recipes!</h1>
                <nav>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/new">Create</Link></li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default NavbarComponent;