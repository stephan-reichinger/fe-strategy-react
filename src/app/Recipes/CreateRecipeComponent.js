import React from 'react';
import ReactDOM from 'react-dom';

class CreateRecipeComponent extends React.Component{
    constructor(props) {
        super(props);
    }

    createNewRecipe(e) {
        e.preventDefault();
        var title = ReactDOM.findDOMNode(this.refs.title).value.trim();
        var author = ReactDOM.findDOMNode(this.refs.author).value.trim();
        var description = ReactDOM.findDOMNode(this.refs.description).value.trim();

        var recipe = {
            id: Date.now(),
            name: title,
            date: '08.03.2016 14:33',
            author: author,
            rating: 0,
            comments: 0,
            description: description
         };

        var data = JSON.stringify(recipe);

        return fetch('http://localhost:3000/recipes', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: data
        })
        .then(function(res){ return res.json(); })
        .then(function(){
            document.getElementById("create-recipe").reset();
            alert("new recipe created")
        });
    }

    render() {
        return (
            <div className="content-wrapper">
                <h2>Create new recipe:</h2>
                <article className="create-recipe clearfix">
                    <form id="create-recipe" onSubmit={this.createNewRecipe.bind(this)}>
                        Title:
                        <input type="text" id="name" name="title" ref="title"/><br />
                            Author:
                            <input type="text" id="author" name="author" ref="author" /><br />
                            Short description:<br />
                            <textarea id="short-description" rows="10" cols="30" ref="description" />
                            <br />
                            <button type="submit" value="post" form="create-recipe" id="recipe-submit">Submit</button>
                    </form>
                </article>
            </div>
        )
    }
}

export default CreateRecipeComponent;