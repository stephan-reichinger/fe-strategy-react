import React from 'react';
import { Router, Route, Link } from 'react-router'

class RecipeComponent extends React.Component{

    render() {
        var divStyle = {
            backgroundImage: 'url("images/yummy.jpg")'
        };
        return (
            <div className="recipe-preview clearfix">
                <div className="thumbnail-wrapper">
                    <a href="recipe.html">
                        <div className="thumbnail" style={divStyle}>
                        </div>
                    </a>
                </div>
                <div className="text-wrapper">
                    <h3><Link to="detail">{this.props.displayName}</Link></h3>
                    <p className="info"><span>{this.props.datum}</span> by <span className="author">{this.props.author}</span> | User rating: <span className="rating">{this.props.rating}</span> | Comments: <span className="number-of-comments">{this.props.comments}</span></p>
                    <p className="short-description">
                        {this.props.description}
                    </p>
                    <p className="actions">
                        <span className="button"><Link to="detail">Read More</Link></span>
                        <span className="button">Rating +1</span>
                    </p>
                </div>
            </div>
        );
    }
}

export default RecipeComponent;