import React from 'react';
import ReactDOM from 'react-dom';

class RecipeDetailComponent extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        var divStyle = {
            backgroundImage: 'url("images/yummy.jpg")'
        };
        return (
            <div className="content-wrapper">
                <h2>Yummy Burger</h2>
                <article className="recipe clearfix">

                    <div className="picture" style={divStyle}>
                    </div>
                    <div className="text-wrapper">
                        <p className="info"><time datetime="2016-02-14 08:00">14.02.2016 | 08:00</time> by <span className="author">grandma876</span> | User rating: <span className="rating">0</span> | Comments: <span className="number-of-comments">0</span></p>
                        <div className="description">
                        </div>
                        <p className="actions">
                            <span className="button">Rating +1</span>
                            <span className="button">Edit</span>
                            <span className="button">Delete</span>
                        </p>
                    </div>
                </article>
            </div>
        )
    }
}

export default RecipeDetailComponent;