import React from 'react';
import Recipe from './RecipeComponent.js';

class RecipeListComponent extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        var recipes = this.props.data.map((recipe) => {
            return (
                <Recipe key={recipe.id} displayName={recipe.name} author={recipe.author} datum={recipe.date} rating={recipe.rating}
                        description={recipe.description} comments={recipe.comments}/>
            );
        });
        return (
            <div>
                {recipes}
            </div>
        );
    }
}

export default RecipeListComponent;