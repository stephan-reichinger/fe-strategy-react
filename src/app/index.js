import React from 'react';
import {render} from 'react-dom';
import { Router, Route, Link } from 'react-router'
import Content from './ContentComponent.js';
import CreateRecipe from './Recipes/CreateRecipeComponent';
import RecipeDetail from './Recipes/RecipeDetailComponent';
import Navbar from './NavbarComponent.js';

require('../styles/styles.css');

class App extends React.Component {
    render () {
        return (
            <div>
                <Navbar />
                <Content />
            </div>
        );
    }
}

class Create extends React.Component {
    render () {
        return (
            <div>
                <Navbar />
                <CreateRecipe />
            </div>
        );
    }
}

class Detail extends React.Component {
    render () {
        return (
            <div>
                <Navbar />
                <RecipeDetail />
            </div>
        );
    }
}

render(<Router>
            <Route path="/" component={App} />
            <Route path="/new" component={Create}/>
            <Route path="/detail" component={Detail}/>
        </Router>
    , document.getElementById('app'));